export const createAsyncDelay = duration => {
    return new Promise((resolve, _) => setout(() => { resolve(); }, duration));
};