import { BlockPrevActions } from '@psmi-storage/realm';

export const TaskOrder = {
    /* Add Labor to Task Order
     * @param => task order, labors
    */
    addLabor: (taskOrder, labors) => {
        let { message, flag } = existsWorkerOnTO(taskOrder, labors);
        if(!flag){
            taskOrder['labors'] = taskOrder.labors.concat(arrayUnique(taskOrder.labors, labors));
        }

        return {data: taskOrder, message};
    },
    /* Update Labors on Task Order
     * @param => task order, labor
    */
    updateLabor: (taskOrder, newLabor) => {
        let index = taskOrder.labors.map((labor) => labor.id).indexOf(newLabor.id);
        if(index !== -1) taskOrder.labors[index] = newLabor;
        taskOrder['labors'] = taskOrder.labors;
        
        return {...taskOrder, _refresh: taskOrder._refresh ? false : true};
    },
    /* Add Material to Task Order
     * @param => task order, materials
    */
    addMaterial: (taskOrder, materials) => {
        taskOrder['materials'] = taskOrder.materials.concat(arrayUnique(taskOrder.materials, materials));

        return taskOrder;
    },
    /* Update Materials on Task Order
     * @param => task order, material
    */
    updateMaterial: (taskOrder, newMaterial) => {
        let index = taskOrder.materials.map((material) => material.id).indexOf(newMaterial.id);
        if(index !== -1) taskOrder.materials[index] = newMaterial;
        taskOrder['materials'] = taskOrder.materials;
        
        return {...taskOrder, _refresh: taskOrder._refresh ? false : true};
    },
    /* Add EOC to Task Order
     * @param => task order, eoc
    */
    addEOC: (taskOrder, eoc) => {
        taskOrder['eoc'] = taskOrder.eoc.concat(arrayUnique(taskOrder.eoc, eoc));

        return taskOrder;
    },
    /* Add Contract to Task Order
     * @param => task order, contracts
    */
    addContract: (taskOrder, contracts) => {
        taskOrder['contracts'] = taskOrder.contracts.concat(contracts);

        return taskOrder;
    },
    /* Update Contracts on Task Order
     * @param => task order, contract (vendor)
    */
    updateContract: (taskOrder, newContract) => {
        let index = newContract.index;
        if(index !== -1) taskOrder.contracts[index] = newContract;

        taskOrder['contracts'] = taskOrder.contracts;

        return {...taskOrder, _refresh: taskOrder._refresh ? false : true};
    },
    removeDataContract: (taskOrder, contract) => {
        let index = contract.index;
        if(index !== -1) taskOrder['contracts'].splice(index, 1);

        return {...taskOrder, _refresh: taskOrder._refresh ? false : true};
    },
    /* Add Insentif to Task Order
     * @param => task order, insentif data
    */
    addInsentif: (taskOrder, insentif) => {

        taskOrder['insentif'] = taskOrder.insentif.concat(arrayUnique(taskOrder.insentif, insentif));

        return taskOrder;
    },
    /* Add Block to Task Order
     * @param => task order, blocks
    */
    addBlock: (taskOrder, blocks) => {
        let formatBlocks = blocks;
        formatBlocks.map((block) => {
            let period = taskOrder?.confirmdate != null ? new Date(taskOrder.confirmdate).getFullYear() : new Date().getFullYear(); //dari confirmdate
            let canecategoryid = taskOrder?.canecategoryid != null ? taskOrder.canecategoryid : 0;
            block.prev_balance = 0;
            let data = BlockPrevActions.findPrevBlockByParams(block.id, canecategoryid, taskOrder.sub_activity.id, `${period}`)
            BlockPrevActions.retrieveAll().then((data) => {
            })
            if(data.length > 0){
                block.prev_balance = data[0].prevbalance > 0 ? data[0].prevbalance.toFixed(2) : 0;
            }
        });
        taskOrder['blocks'] = taskOrder.blocks.concat(arrayUnique(taskOrder.blocks, formatBlocks));
        return taskOrder;
    },
    /* Update Blocks on Task Order
     * @param => task order, block
    */
    updateBlock: (taskOrder, newBlock) => {
        let index = taskOrder.blocks.map((block) => block.id).indexOf(newBlock.id);
        if(index !== -1) taskOrder.blocks[index] = newBlock;
        taskOrder['blocks'] = taskOrder.blocks;
        
        return {...taskOrder, _refresh: taskOrder._refresh ? false : true};
    },

    updateBlockList: (taskOrder, newBlocks) => {
        taskOrder['blocks'] = newBlocks;

        return {...taskOrder, _refresh: taskOrder._refresh ? false : true};
    },
    removeDataById: (taskOrder, item, model) => {
        let index = taskOrder[model].map((item) => item.id).indexOf(item.id);
        if(index !== -1) taskOrder[model].splice(index, 1);

        return {...taskOrder, _refresh: taskOrder._refresh ? false : true};
    },
    /* Update EOC on Task Order
     * @param => task order, material
    */
    updateEOC: (taskOrder, newEOC) => {
        let index = taskOrder.eoc.map((eoc) => eoc.id).indexOf(newEOC.id);
        if(index !== -1) taskOrder.eoc[index] = newEOC;
        taskOrder['eoc'] = taskOrder.eoc;
        
        return {...taskOrder, _refresh: taskOrder._refresh ? false : true};
    },
    
}

export const TaskOrderList = {
    update: (list, row) => {
        let index = list.map((item) => item.task_order_id).indexOf(row.task_order_id);
        if(index !== -1) list[index] = row;
        
        return list;
    },
}

//vendor
export const Contract = {
    /* Add Supervisor to Contract
     * @param => contract, crew with type [pengawas]
    */
    addCrew: (taskOrder, contract, crew, type) => {
        let message = "Perubahan berhasil disimpan";
        if(contract.crew != null && contract.crew.length > 0){
            let newArray = crew;
            let currentArray = contract.crew;
            let result = [];
            if(type == 'staff'){
                newArray.map((newItem) => {
                    let index = currentArray.map(currentItem => currentItem.id).indexOf(newItem.id);
                    if(index > -1){
                        let crew = currentArray.filter((item) => (item.id == newItem.id && item.type == newItem.type));
                        if(crew.length == 0) result.push(newItem);
                    }else{
                        result.push(newItem);
                    }
                });
                result.map((item) => {
                    currentArray.push(item);
                })
                contract['crew'] = currentArray;
            }else{
                let flag = true;
                let allContracts = taskOrder.contracts.filter((item) => item.id == contract.id);
                allContracts.map((cntr) => {
                    let isSPVExists = cntr.crew != null ? cntr.crew.filter((cntrCrw) => cntrCrw.type == 'pengawas' && cntrCrw.id == crew[0].id).length : 0;
                    console.log(isSPVExists)
                    if(isSPVExists > 0) flag = false;
                })
                if(flag){
                    let index = currentArray.map((item) => item.type).indexOf('pengawas');
                    if(index > -1){
                        currentArray[index] = crew[0];
                    }else{
                        currentArray.push(crew[0]);
                    }
                    contract['crew'] = currentArray;
                }else{
                    message = "Gagal menambahkan SPV.";
                }
            }
        }else{
            let flag = true;
            if(type == 'pengawas'){
                let allContracts = taskOrder.contracts.filter((item) => item.id == contract.id);
                allContracts.map((cntr) => {
                    let isSPVExists = cntr.crew != null ? cntr.crew.filter((cntrCrw) => cntrCrw.type == 'pengawas' && cntrCrw.id == crew[0].id).length : 0;
                    console.log(isSPVExists)
                    if(isSPVExists > 0) {
                        flag = false;
                        message = "Gagal menambahkan SPV.";
                    }
                });
            }
            if(flag) contract = {...contract, crew: crew}
        }
        return {contract, message};
    },
    removeCrew: (contract, item) => {
        let index = contract.crew.map((item) => item.id).indexOf(item.id);
        if(index !== -1) contract.crew.splice(index, 1);

        return contract;
    },
}

//resource
export const EOC = {
    addDriver: (taskOrder, eoc, drivers) => {
        let newEOC = eoc;
        let { message, flag } = existsWorkerOnTO(taskOrder, drivers);
        if(!flag){
            if(newEOC.drivers != undefined && newEOC.drivers.length > 0){
                newEOC['drivers'] = newEOC.drivers.concat(arrayUnique(newEOC.drivers, drivers));
            }else{
                newEOC = {...eoc, drivers: drivers};
            }
        }
    
        return {eoc: newEOC, message};
    },
    updateDriver: (eoc, newDriver) => {
        let index = eoc.drivers.map((item) => item.id).indexOf(newDriver.id);
        if(index !== -1) eoc.drivers[index] = newDriver;
        eoc['drivers'] = eoc.drivers;
        
        return eoc;
    },
    removeDriver: (eoc, item) => {
        let index = eoc.drivers.map((item) => item.id).indexOf(item.id);
        if(index !== -1) eoc.drivers.splice(index, 1);

        return eoc;
    },
    addImplement: (eoc, implementsEOC) => {
        let newEOC = eoc;
        if(newEOC.implements != undefined && newEOC.implements.length > 0){
            newEOC['implements'] = newEOC.implements.concat(arrayUnique(newEOC.implements, implementsEOC));
        }else{
            newEOC = {...eoc, implements: implementsEOC};
        }
        return newEOC;
    },
    removeImplement: (eoc, item) => {
        let index = eoc.implements.map((item) => item.id).indexOf(item.id);
        if(index !== -1) eoc.implements.splice(index, 1);

        return eoc;
    },
}

function existsWorkerOnTO(TO, workers){
    let flag = false;
    let message = `Berhasil menambahkan worker`;
    const labors = TO.labors;
    const eoc = TO.eoc;

    workers.map((worker) => {
        if(labors != null && labors.length > 0){
            let indexLabor = labors.map((labor) => labor.id).indexOf(worker.id);
            if(indexLabor > -1){
                flag = true;
                message = `Gagal menabahkan worker : ${worker.id} - ${worker.name}, Data worker sudah digunakan pada menu Labor. `;
                return {flag: message};
            }
        }
        if(eoc != null) {
            eoc.map((eocItem) => {
                if(eocItem.drivers != null && eocItem.drivers.length > 0){
                    let indexEoc = eocItem.drivers.map((driver) => driver.id).indexOf(worker.id);
                    if(indexEoc > -1) {
                        flag = true;
                        message = `Gagal menabahkan worker : ${worker.id} - ${worker.name}, Data worker sudah digunakan pada menu EOC : ${eocItem.id} - ${eocItem.name}. `;
                        return {flag: message};
                    }
                }
            })
        }
    })

    return {flag, message};
}

function arrayUnique(currentArray, newArray) {
    let result = [];

    newArray.map((newItem) => {
        let index = currentArray.map(currentItem => currentItem.id).indexOf(newItem.id);
        if(index == -1) result.push(newItem);
    })

    return result;
}