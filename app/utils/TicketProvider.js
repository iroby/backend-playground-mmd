import axios from 'axios';
import { 
    WorkerActions,
    VendorActions,
    EOCActions,
    InsentifActions
 } from '@psmi-storage/realm';

const baseUrl = 'https://app.psmi.co.id';

export const Ticket = {
    addMasterData: (ticket, selectedData, model) => {
        const newTicket = {...ticket};
        switch (model) {
            case 'divisi':
                delete newTicket.sub_divisi;
                delete newTicket.wilayah;
                delete newTicket.block;
                break;
            case 'sub_divisi':
                delete newTicket.wilayah;
                delete newTicket.block;
                break;
            case 'wilayah':
                delete newTicket.block;
                break;
            case 'vendor_tebang':
                delete newTicket.kepala_rombong;
                break;
            case 'vendor_truck':
                delete newTicket.truck_no;
                break;
        }
        newTicket[model] = selectedData;

        return newTicket;
    },
}

export const Divisi = {
    /*
     * @params => site
    */
    fetchData: (site = 'psmi') => {
        return axios.get(`${baseUrl}/api/synchronize/wbdivisi`, {
            params: {
                site: site
            }
        });
    }
}

export const KepalaRombong = {
    /*
     * @params => vendaccount
    */
    fetchData: (vendaccount) => {
        return axios.get(`${baseUrl}/api/synchronize/kepalarombong3?vendaccount=${vendaccount}`);
    }
}

export const SubDivisi = {
    /*
     * @params => divisiid
    */
   fetchData: (divisiid) => {
        return axios.get(`${baseUrl}/api/synchronize/wbsubdivisi`, {
            params: {
                divisiid: divisiid
            }
        });
    }
}

export const Wilayah = {
    /*
     * @params => subdiv
    */
   fetchData: (subdiv) => {
        return axios.get(`${baseUrl}/api/synchronize/wbwilayah`, {
            params: {
                subdiv: subdiv
            }
        });
    }
}

export const Operator = {
    getData: () => {
        return WorkerActions.retrieveAll();
    },
}

export const Insentif = {
    getData: () => {
        return InsentifActions.retrieveAll();
    },
    fetchData: (last_synch = '2018-01-01') => {
        return axios.get(`${baseUrl}/api/synchronize/wbInsentif`, {
            params: {
                last_synch: last_synch
            }
        });
    }
}

export const EOC = {
    getData: () => {
        return EOCActions.retrieveAll();
    },
}

export const Driver = {
    getData: () => {
        return WorkerActions.retrieveAll();
    },
    fetchData: (last_synch = '2018-01-01') => {
        return axios.get(`${baseUrl}/api/synchronize/wbDriver`, {
            params: {
                last_synch: last_synch
            }
        });
    }
}

export const VendorTebang = {
    fetchData: () => {
        return axios.get(`${baseUrl}/api/synchronize/wbVendorTebang`, {
            params: {
                // subdiv: subdiv
            }
        });
    }
}

export const VendorTruck = {
    fetchData: () => {
        return axios.get(`${baseUrl}/api/synchronize/wbVendorTruck`, {
            params: {
                // subdiv: subdiv
            }
        });
    }
}

export const Truck = {
    /*
     * vendorid = wbTruckId
    */
    fetchData: (vendorid, last_synch = '2018-01-01') => {
        return axios.get(`${baseUrl}/api/synchronize/truckTable`, {
            params: {
                last_synch: last_synch,
                vendorid: vendorid
            }
        });
    }
}

export const Block = {
    /*
     * @params => subdiv
    */
   fetchData: (wilayah) => {
        return axios.get(`${baseUrl}/api/synchronize/wbBlockPSMI2`, {
            params: {
                wilayah: wilayah
            }
        });
    }
}

function arrayUnique(currentArray, newArray) {
    let result = [];

    newArray.map((newItem) => {
        let index = currentArray.map(currentItem => currentItem.id).indexOf(newItem.id);
        if(index == -1) result.push(newItem);
    })

    return result;
}