/**
 * Typography:
 * This contains all the typography config for the application
 * #Note: color and font size are defaulted as they can be overridden
 *        as required.
 */

const Typography = {
    Heading: {
        regular: {
            fontSize: 22,
            fontFamily: 'SFProDisplay-Bold',
        },
        bold: {
            fontSize: 22,
            fontWeight: 'bold',
            fontFamily: 'SFProDisplay-Bold',
        },
    },
    Body: {
        regular: {
            fontSize: 18,
            fontFamily: 'SFProDisplay-Regular',
        },
        bold: {
            fontSize: 18,
            fontFamily: 'SFProDisplay-Regular',
            fontWeight: 'bold'
        },
        light: {
            fontSize: 16,
            fontFamily: 'SFProDisplay-Light',
            color: '#000'
        }
    },
    Caption: {
        thin: {
            fontSize: 12,
            fontFamily: 'SFProDisplay-Thin',
            color: '#000'
        }
    },
};

export default Typography;
